angular.module('demo', [
	'forge.datatables'
])

.config(['DTConfigProvider', function(DTConfigProvider) {
	DTConfigProvider.setSpinnerImage('spinner.gif');
}])

.controller('DemoCtrl', ['$scope', 'DemoService', 'DTOptionsBuilder', 'DTColumnBuilder', function ($scope, DemoService, DTOptionsBuilder, DTColumnBuilder) {
	// the DataTables config
	$scope.dtOptions = DTOptionsBuilder.withDefaultOptions()
		.withServiceMethod(DemoService.getTableData)
		.withBootstrap()
	;
	// the DataTable column config
	$scope.dtColumns = [
		DTColumnBuilder.addColumn('id', 'ID').withWidth('20%'),
		DTColumnBuilder.addColumn('firstname', 'Name').withWidth('40%').renderWith(function(value, type, object) {
			return object.firstname + ' ' + object.lastname;
		}),
		DTColumnBuilder.addColumn('email', 'Email').withWidth('40%').notSortable()
	];

	$scope.demo1 = {
		markup: true,
		javascript: false
	};
	$scope.toggleDemo1 = function(e, markup) {
		e.preventDefault();
		if (markup) {
			$scope.demo1.javascript = false;
			$scope.demo1.markup = true;
		} else {
			$scope.demo1.markup = false;
			$scope.demo1.javascript = true;
		}
	};
}])

.factory('DemoService', ['$timeout', function($timeout) {
	var demoData = [
		{
			id: 1,
			firstname: 'John',
			lastname: 'Smith',
			email: 'john@example.com'
		}, 
		{
			id: 2,
			firstname: 'Jane',
			lastname: 'Smith',
			email: 'jane@example.com'
		}
	];

	var DemoResource = function() {
		this.getTableData = function(data, callback) {
			$timeout(function() {
				callback({
					"aaData": demoData,
					"iTotalDisplayRecords": 2,
					"iTotalRecords": 2,
					"sEcho": data.sEcho
				});
			}, 0, false);
		};
	};

	return new DemoResource();
}])
;