/**
 * Custom AngularJS 1.2 module for Datatables.
 * Part of DANU's internal Forge JS library.
 *
 * Originally based on the angular-datatables module:
 * https://github.com/l-lin/angular-datatables
 */
angular.module('forge.datatables', [
	// Module children
	'datatables.factory',
	'datatables.directive'
])

/**
 * A provier that allows the spinner image to be set during configuration.
 */
.provider('DTConfig', function() {
	var spinnerImg = 'assets/img/spinner.gif';

	this.$get = function() {
		return {
			getSpinnerImg: function() {
				return spinnerImg;
			}
		};
	};

	this.setSpinnerImage = function(newImg) {
		spinnerImg = newImg;
	};
})

;

/**
 * Add a delay to the input filter before updating the table.
 * Ideally this should be written in AngularJS and not jQuery
 */
$.fn.dataTableExt.oApi.fnSetFilteringDelay = function(oSettings, iDelay) {
	var _that = this;
	if (iDelay === undefined) {
		iDelay = 250;
	}

	this.each(function(i) {
		$.fn.dataTableExt.iApiIndex = i;
		var oTimerId = null,
			sPreviousSearch = null,
			anControl = $('input', _that.fnSettings().aanFeatures.f || '.dataTables_filter')
		;
		anControl.unbind('keyup').bind('keyup', function() {
			if (sPreviousSearch === null || sPreviousSearch !== anControl.val()) {
				window.clearTimeout(oTimerId);
				sPreviousSearch = anControl.val();
				oTimerId = window.setTimeout(function() {
					$.fn.dataTableExt.iApiIndex = i;
					_that.fnFilter(anControl.val());
				}, iDelay);
			}
		});
		return this;
	});
	return this;
};