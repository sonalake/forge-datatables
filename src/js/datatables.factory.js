/**
 * Factories to help build the DataTables options and DataTable Column options.
 */
angular.module('datatables.factory', [
	'datatables.bootstrap'
])

// Default options for DataTables
.value('DTDefaultOptions', {
	'sDom': '<\'row\'<\'col-xs-12\'f>r><\'table-responsive\'t><\'row\'<\'col-xs-6\'i><\'col-xs-6\'p>>',
	'oLanguage': {
		'sSearch': '',
		'sNext': '&raquo;',
		'sPrevious': '&laquo;'
	},
	'bServerSide': true,
	'bAutoWidth': false,
	'filterPlaceholder': 'Filter',
	'filterHtml': '',
	'noSpinner': false,
	'extraParams': []
})

// Builds the options object needed for Datatables.
.factory('DTOptionsBuilder', ['DTBootstrap', 'DTDefaultOptions', function(DTBootstrap, DTDefaultOptions) {
	/**
	 * Returns an object containing the request data.
	 * @param data the request data
	 * @param options an instance of DTOptions
	 */
	var transformRequestData = function(data, options) {
		var result = {}, length = data.length;
		for (var i = 0; i < length; i++) {
			result[data[i].name] = data[i].value;
		}
		if (options.extraParams) {
			var extras = options.extraParams;
			length = extras.length;
			for (var j = 0; j < length; j++) {
				result[extras[j].name] = extras[j].value;
			}
		}
		return result;
	},
	/**
	 * The datatables options class
	 * @param sAjaxSource the ajax source to fetch the data
	 * @param dataPromise the promise to fetch the data
	 */
	DTOptions = function() {
		// indicates if the table data needs to be reloaded
		this.reload = false;
		// toggles the reload indicator
		this.reloadData = function () {
			this.reload = true;
			return this;
		};
		// sets the values of extraParams on the options object
		this.setExtraParams = function (extras) {
			this.extraParams = extras || [];
			return this;
		};
		/**
		 * Add the option to the datatables optoins
		 * @param key the key of the option
		 * @param value an object or a function of the option
		 * @returns {DTOptions} the options
		 */
		this.withOption = function(key, value) {
			if (!angular.isString(key)) {
				throw new Error('The key must be a string');
			}
			this[key] = value;
			return this;
		};
		/**
		 * Set the placeholder text for the filter input.
		 * This is not an actual datatables option, but a custom option for this module.
		 * @param placeholder the placeholder text to use
		 * @returns {DTOptions} the options
		 */
		this.withPlaceholderText = function(placeholder) {
			if (!angular.isString(placeholder)) {
				throw new Error('The placeholder text must be a String');
			}
			this.filterPlaceholder = placeholder;
			return this;
		};
		/**
		 * Set the row click callback function.
		 * This is not an actual datatables option, but a custom option for this module.
		 * @param fn the function for the callback
		 * @returns {DTOptions} the options
		 */
		this.withFnRowClickCallback = function(fn) {
			if (!angular.isFunction(fn)) {
				throw new Error('The parameter must be a function');
			}
			this.fnRowClickCallback = fn;
			return this;
		};
		/**
		 * Set the pagination type.
		 * @param sPaginationType the pagination type
		 * @returns {DTOptions} the options
		 */
		this.withPaginationType = function(sPaginationType) {
			if (!angular.isString(sPaginationType)) {
				throw new Error('The pagination type must be provided');
			}
			this.sPaginationType = sPaginationType;
			return this;
		};
		/**
		 * Set the language of the datatables
		 * @param oLanguage the language
		 * @returns {DTOptions} the options
		 */
		this.withLanguage = function(oLanguage) {
			this.oLanguage = oLanguage;
			return this;
		};
		/**
		 * Set default number of items per page to display
		 * @param iDisplayLength the number of items per page
		 * @returns {DTOptions} the options
		 */
		this.withDisplayLength = function(iDisplayLength) {
			this.iDisplayLength = iDisplayLength;
			return this;
		};
		/**
		 * Add the ajax data properties.
		 * @param sAjaxDataProp the ajax data property
		 * @returns {DTOptions} the options
		 */
		this.withDataProp = function(sAjaxDataProp) {
			this.sAjaxDataProp = sAjaxDataProp;
			return this;
		};
		/**
		 * Set the server data function.
		 * @param fn the function of the server retrieval
		 * @returns {DTOptions} the options
		 */
		this.withFnServerData = function(fn) {
			if (!angular.isFunction(fn)) {
				throw new Error('The parameter must be a function');
			}
			this.fnServerData = fn;
			return this;
		};
		/**
		 * Uses the $resource service as a data source for fnServerData.
		 * @param fn the $resource service method to use
		 * @returns {DTOptions} the options
		 */
		this.withServiceMethod = function(fn) {
			var options = this;
			if (!angular.isFunction(fn)) {
				throw new Error('The parameter must be a function');
			}
			return this.withFnServerData(function(sSource, aoData, fnCallback) {
				fn(transformRequestData(aoData, options), function(json) {
					fnCallback(json);
				});
			});
		};
		/**
		 * Set the pre draw callback function.
		 * @param fn the function for the callback
		 * @returns {DTOptions} the options
		 */
		this.withFnPreDrawCallback = function(fn) {
			if (!angular.isFunction(fn)) {
				throw new Error('The parameter must be a function');
			}
			this.fnPreDrawCallback = fn;
			return this;
		};
		/**
		 * Set the draw callback function.
		 * @param fn the function for the callback
		 * @returns {DTOptions} the options
		 */
		this.withFnDrawCallback = function(fn) {
			if (!angular.isFunction(fn)) {
				throw new Error('The parameter must be a function');
			}
			this.fnDrawCallback = fn;
			return this;
		};
		/**
		 * Add bootstrap compatibility
		 * @returns {DTOptions} the options
		 */
		this.withBootstrap = function() {
			DTBootstrap.integrate(this);
			return this;
		};
		/**
		 * Add extra filters to the filter area
		 * @param html the html of the additional filters (will go through $compile)
		 * @returns {DTOptions} the options
		 */
		this.withAdditionalFilters = function(html) {
			this.filterHtml = html;
			return this;
		};
		/**
		 * Toggles whether a spinner gif should be shown in the filter area.
		 * @param noSpinner set to true if you want to remove the html for adding a spinner
		 * @returns {DTOptions} the options
		 */
		this.withoutSpinner = function(noSpinner) {
			this.noSpinner = noSpinner;
			return this;
		};
	};

	// Return the DTOptionsBuilder
	return {
		newOptions: function() {
			return new DTOptions();
		},
		withDefaultOptions: function() {
			return angular.extend(new DTOptions(), DTDefaultOptions);
		}
	};
}])

// Builds a column option object needed by Datatables when defining a column.
.factory('DTColumnBuilder', function() {
	/**
	 * The datatables column 
	 * @param mData the data to display of the column
	 * @param sTitle the sTitle of the column title to display in the DOM
	 */
	var DTColumn = function(mData, sTitle) {
		if (angular.isUndefined(mData)) {
			throw new Error('The parameter "mData" is not defined!');
		}
		this.mData = mData;
		this.sTitle = sTitle || '';
		/**
		 * Add the option of the column
		 * @param key the key of the option
		 * @param value an object or a function of the option
		 * @returns {DTColumn} the wrapped datatables column
		 */
		this.withOption = function(key, value) {
			if (!angular.isString(key)) {
				throw new Error('The key must be a string');
			}
			this[key] = value;
			return this;
		};
		/**
		 * Set the title of the colum
		 * @param sTitle the sTitle of the column
		 * @returns {DTColumn} the wrapped datatables column
		 */
		this.withTitle = function(sTitle) {
			this.sTitle = sTitle;
			return this;
		};
		/**
		 * Set the CSS class of the column
		 * @param sClass the CSS class
		 * @returns {DTColumn} the wrapped datatables column
		 */
		this.withClass = function(sClass) {
			this.sClass = sClass;
			return this;
		};
		/**
		 * Set the width of the column
		 * @param sWidth the width as a string
		 * @returns {DTColumn} the wrapped datatables column
		 */
		this.withWidth = function(sWidth) {
			this.sWidth = sWidth;
			return this;
		};
		/**
		 * Hide the column
		 * @returns {DTColumn} the wrapped datatables column
		 */
		this.notVisible = function() {
			this.bVisible = false;
			return this;
		};
		/**
		 * Set the column as not sortable
		 * @returns {DTColumn} the wrapped datatables column
		 */
		this.notSortable = function() {
			this.bSortable = false;
			return this;
		};
		/**
		 * Render each cell with the given parameter
		 * @mRender mRender the function/string to render the data
		 * @returns {DTColumn} the wrapped datatables column
		 */
		this.renderWith = function(mRender) {
			this.mRender = mRender;
			return this;
		};
	};
	
	// Return the DTColumnBuilder
	return {
		addColumn: function(mData, sTitle) {
			return new DTColumn(mData, sTitle);
		}
	};
})

;