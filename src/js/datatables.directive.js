/**
 * The directive for actually adding a DataTable
 */
angular.module('datatables.directive', [])

.directive('datatable', ['DTConfig', '$timeout', '$compile', function(DTConfig, $timeout, $compile) {
		// A reference to the datatable object
	var oTable = null,
		// Adds callback methods for fnPreDrawCallback and fnDrawCallback if they aren't already set
		addPreAndPostCallbacks = function($elem, options, $scope) {
			// This needs to be done here so the methods can access oTable.

			// If there's no predefined fnPreDrawCallback method, then set this one
			if (!angular.isFunction(options.fnPreDrawCallback)) {
				options.fnPreDrawCallback = function() {
					$scope.dtOptions.isLoading = true;
				};
			}
			// If there's no predefined fnDrawCallback method, then set this one
			if (!angular.isFunction(options.fnDrawCallback)) {
				options.fnDrawCallback = function() {
					$scope.dtOptions.isLoading = false;
					$scope.dtOptions.iRecordsDisplay = oTable.fnSettings().fnRecordsDisplay();

					// Show/hide the pagination options depending on the total number of items in the table
					if(Math.ceil((oTable.fnSettings().fnRecordsDisplay()) / oTable.fnSettings()._iDisplayLength) > 1 ) {
						$('.dataTables_paginate').removeClass('hide');
					} else {
						$('.dataTables_paginate').addClass('hide');
					}
				};
			}
		},
		// Modify the generate HTML for datatables to have better Bootstrap styling
		modifyDatatableHtml = function($elem, options, $scope) {
			// remove the <label> around the input
			$('.dataTables_filter > label').replaceWith(function() {
				return $('input', this);
			});
			// add the icon before the input (bootstrap styling)
			$('.dataTables_filter input').before('<span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>');
			// set the input's placeholder text
			$('.dataTables_filter input').attr('placeholder', options.filterPlaceholder).addClass('form-control');
			// add the loading spinner, which needs to be $compiled to make use of the scope
			$('.dataTables_filter').after($compile(options.filterHtml + (options.noSpinner?'':'<img class="spinner" ng-show="dtOptions.isLoading" src="'+DTConfig.getSpinnerImg()+'">'))($scope));
		},
		// If there is a callback defined for clicking a row, then here is where it will be added
		addRowClickCallback = function($elem, options) {
			if (angular.isFunction(options.fnRowClickCallback)) {
				angular.element('#'+$elem[0].id+' tbody').on('click', 'tr', function() {
					options.fnRowClickCallback(oTable.fnGetData(this), oTable.fnGetPosition(this), this);
				});
			}
		},
		// Render the table via a $timeout to be sure that angular has finished rendering before calling datatables
		initDataTable = function($elem, options, $scope) {
			$timeout(function() {
				addPreAndPostCallbacks($elem, options, $scope);
				oTable = $elem.dataTable(options).fnSetFilteringDelay();
				modifyDatatableHtml($elem, options, $scope);
				// Show $elem
				$elem.show();
				addRowClickCallback($elem, options, $scope);
			}, 0, false);
		}
	;

	return {
		restrict: 'A',
		scope: {
			dtOptions: '=',
			dtColumns: '='
		},
		link: function($scope, $elem) {
			// Hide the table
			$elem.hide();
			// Build options
			var options;
			if (angular.isDefined($scope.dtOptions)) {
				options = {};
				angular.extend(options, $scope.dtOptions);
				// Set the columns
				if (angular.isArray($scope.dtColumns)) {
					options.aoColumns = $scope.dtColumns;
				}
			}
			// Init datatables
			initDataTable($elem, options, $scope);
			// Add watchers
			$scope.$watch('dtOptions.reload', function (reload) {
				if (reload) {
					$scope.dtOptions.reload = false;
					if (oTable) {
						oTable.fnDraw(false);
					}
				}
			});
			// When the table is destroyed...
			$scope.$on('$destroy', function() {
				oTable.fnDestroy();
			});
		}
	};
}])

;