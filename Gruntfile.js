'use strict';

/*global module:false*/
module.exports = function(grunt) {

	// Load grunt tasks automatically
	require('load-grunt-tasks')(grunt);

	// Time how long tasks take. Can help when optimizing build times
	require('time-grunt')(grunt);

	// Project configuration.
	grunt.initConfig({
		// definition of pkg variable
		pkg: grunt.file.readJSON('package.json'),
		// the directory containing the source code
		appDir: 'src',
		// the directory containing the distribution code
		buildDir: 'dist',
		// definition of banner variable
		banner: '/*! <%= pkg.name %> - <%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
			' * Copyright (c) <%= grunt.template.today("yyyy") %> DANU Technologies;\n' +
			' */\n',
		
		// Empties dist directories
		clean: ['<%= buildDir %>'],
		// configuration for concatting all the src files together
		concat: {
			options: {
				banner: '<%= banner %>',
				stripBanners: true
			},
			dist: {
				src: ['<%= appDir %>/helpers/intro.js', '<%= appDir %>/js/**/*.js', '<%= appDir %>/helpers/outro.js'],
				dest: '<%= buildDir %>/<%= pkg.name %>.js'
			}
		},
		// configuration for JSHint
		jshint: {
			options: {
				jshintrc: '.jshintrc',
				reporter: require('jshint-stylish')
			},
			all: [
				'Gruntfile.js',
				'<%= buildDir %>/*.js'
			],
			test: {
				options: {
					jshintrc: 'test/.jshintrc'
				},
				src: ['test/e2e/**/*.js']
			}
		},
		// configuration of obfuscating the code
		uglify: {
			options: {
				banner: '<%= banner %>'
			},
			dist: {
				files: {
					'<%= buildDir %>/<%= pkg.name %>.min.js': ['<%= buildDir %>/<%= pkg.name %>.js']
				}
			}
		},
		// compiles the SCSS into CSS
		sass: {
			dist: {
				options: {
					banner: '/*! <%= pkg.name %> - <%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
					' * Copyright (c) <%= grunt.template.today("yyyy") %> DANU Technologies;\n' +
					' */',
					style: 'expanded'
				},
				files: {
					'<%= buildDir %>/forge-datatables.css': '<%= appDir %>/scss/datatables.scss'
				}
			}
		},
		// sets up a local server for runnning tests on
		connect: {
			options: {
				port: 9000,
				// Change this to '0.0.0.0' to access the server from outside.
				hostname: 'localhost'
			},
			test: {
				options: {
					port: 9001,
					base: [
						'.tmp',
						'test',
						'<%= appDir %>'
					]
				}
			}
		},
		// configuration for unit tests
		karma: {
			unit: {
				configFile: 'test/unit.conf.js',
				autoWatch: false,
				singleRun: true
			}
		}
	});

	// Test task
	grunt.registerTask('test', ['jshint:test', 'connect:test', 'karma']);

	// Build task
	grunt.registerTask('build', ['clean', 'concat', 'jshint:all', 'uglify', 'sass']);

	// Default task (i.e. 'grunt')
	grunt.registerTask('default', ['test', 'build']);
};
