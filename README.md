# forge-datatables #
forge-datatables is a AngularJS wrapper for the [DataTables](http://legacy.datatables.net/) library. It is part of the DANU Forge library, our private AngularJS set of plug-ins.
## Requirements ##
* AngularJS 1.2.28
* jQuery 1.10
* DataTables 1.9.4
## Usage ##
We use [bower](http://bower.io/) and [grunt](http://gruntjs.com/) for dependency management. Run:

```
#!cmd

bower install forge-datatables=git@bitbucket.org:danutech/forge-datatables.git#0.1 --save
bower update
```
This will save forge-datatables as a dependency in you grunt project.
Now you can add the module as a dependency to your application module:

```
#!javascript

angular.module('exampleApp', ['forge.datatables']);
```
And then for the table:

```
#!html

<table datatable="" dt-options="dtOptions" dt-columns="dtColumns"></table>
```

```
#!javascript

$scope.dtOptions = DTOptionsBuilder.withDefaultOptions();
$scope.dtColumns = [DTColumnBuilder.addColumn('id', 'ID').withWidth('20%'), ...];
```
## Development ##
When developing on forge-datatables you can use the following commands to build and test.
To build the module:

```
#!cmd

grunt build
```
To run the tests on the module:

```
#!cmd

grunt test
```
To test and then build the module:

```
#!cmd

grunt
```
## Testing ##
We use [karma](http://karma-runner.github.io/0.12/index.html) and [grunt](http://gruntjs.com/) to ensure the quality of the code.

```
#!cmd

npm install -g grunt-cli
npm install
bower install
grunt test
```