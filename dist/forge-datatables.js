/*! forge-datatables - 0.1 - 2015-02-11
 * Copyright (c) 2015 DANU Technologies;
 */
(function(window, $, angular, undefined) {
'use strict';
	
angular.module('datatables.bootstrap', [])

.service('DTBootstrap', ['$rootScope', function($rootScope) {
	var _initialized = false,
		_overrideClasses = function() {
			/* Default class modification*/
			angular.extend($.fn.dataTableExt.oStdClasses, {
				'sWrapper': 'dataTables_wrapper form-inline',
				'sFilter': 'dataTables_filter input-group'
			});
		},
		_overridePagingInfo = function() {
			/* API method to get paging information*/
			$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
				return {
					'iStart': oSettings._iDisplayStart,
					'iEnd': oSettings.fnDisplayEnd(),
					'iLength': oSettings._iDisplayLength,
					'iTotal': oSettings.fnRecordsTotal(),
					'iFilteredTotal': oSettings.fnRecordsDisplay(),
					'iPage': oSettings._iDisplayLength === -1 ? 0 : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
					'iTotalPages': oSettings._iDisplayLength === -1 ? 0 : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
				};
			};
		},
		_overridePagination = function() {
			/* Bootstrap style pagination control*/
			angular.extend($.fn.dataTableExt.oPagination, {
				'bootstrap': {
					fnInit: function(oSettings, nPaging, fnDraw) {
						var oLang = oSettings.oLanguage.oPaginate;
						var fnClickHandler = function(e) {
							e.preventDefault();
							if (oSettings.oApi._fnPageChange(oSettings, e.data.action)) {
								fnDraw(oSettings);
							}
						};
						$(nPaging).append('<ul class="pagination">' + '<li class="prev disabled"><a href="#">' + oLang.sPrevious + '</a></li>' + '<li class="next disabled"><a href="#">' + oLang.sNext + '</a></li>' + '</ul>');
						var els = $('a', nPaging);
						$(els[0]).bind('click.DT', { action: 'previous' }, fnClickHandler);
						$(els[1]).bind('click.DT', { action: 'next' }, fnClickHandler);
					},
					fnUpdate: function(oSettings, fnDraw) {
						$rootScope.$broadcast('datatables.totalRecords', oSettings.fnRecordsDisplay());
						var iListLength = 5;
						var oPaging = oSettings.oInstance.fnPagingInfo();
						var an = oSettings.aanFeatures.p;
						var i, ien, j, sClass, iStart, iEnd, iHalf = Math.floor(iListLength / 2);
						if (oPaging.iTotalPages < iListLength) {
							iStart = 1;
							iEnd = oPaging.iTotalPages;
						} else if (oPaging.iPage <= iHalf) {
							iStart = 1;
							iEnd = iListLength;
						} else if (oPaging.iPage >= oPaging.iTotalPages - iHalf) {
							iStart = oPaging.iTotalPages - iListLength + 1;
							iEnd = oPaging.iTotalPages;
						} else {
							iStart = oPaging.iPage - iHalf + 1;
							iEnd = iStart + iListLength - 1;
						}
						var fnPaging = function(e) {
							e.preventDefault();
							oSettings._iDisplayStart = (parseInt($('a', this).text(), 10) - 1)* oPaging.iLength;
							fnDraw(oSettings);
						};
						for (i = 0, ien = an.length; i < ien; i++) {
							// Remove the middle elements
							$('li:gt(0)', an[i]).filter(':not(:last)').remove();
							// Add the new list items and their event handlers
							for (j = iStart; j <= iEnd; j++) {
								sClass = j === oPaging.iPage + 1 ? 'class="active"' : '';
								$('<li ' + sClass + '><a href="#">' + j + '</a></li>').insertBefore($('li:last', an[i])[0]).bind('click', fnPaging);
							}
							// Add / remove disabled classes from the static elements
							if (oPaging.iPage === 0) {
								$('li:first', an[i]).addClass('disabled');
							} else {
								$('li:first', an[i]).removeClass('disabled');
							}
							if (oPaging.iPage === oPaging.iTotalPages - 1 || oPaging.iTotalPages === 0) {
								$('li:last', an[i]).addClass('disabled');
							} else {
								$('li:last', an[i]).removeClass('disabled');
							}
						}
					}
				}
			});
		},
		_init = function() {
			if (!_initialized) {
				_overrideClasses();
				_overridePagingInfo();
				_overridePagination();
				_initialized = true;
			}
		}
	;
	/**
	 * Integrate Bootstrap
	 * @param options the datatables options
	 */
	this.integrate = function(options) {
		_init();
		options.sPaginationType = 'bootstrap';
	};
}])

;
angular.module('datatables.directive', [])

.directive('datatable', ['DTConfig', '$timeout', '$compile', function(DTConfig, $timeout, $compile) {
		// A reference to the datatable object
	var oTable = null,
		// Adds callback methods for fnPreDrawCallback and fnDrawCallback if they aren't already set
		addPreAndPostCallbacks = function($elem, options, $scope) {
			// This needs to be done here so the methods can access oTable.

			// If there's no predefined fnPreDrawCallback method, then set this one
			if (!angular.isFunction(options.fnPreDrawCallback)) {
				options.fnPreDrawCallback = function() {
					$scope.dtOptions.isLoading = true;
				};
			}
			// If there's no predefined fnDrawCallback method, then set this one
			if (!angular.isFunction(options.fnDrawCallback)) {
				options.fnDrawCallback = function() {
					$scope.dtOptions.isLoading = false;
					$scope.dtOptions.iRecordsDisplay = oTable.fnSettings().fnRecordsDisplay();

					// Show/hide the pagination options depending on the total number of items in the table
					if(Math.ceil((oTable.fnSettings().fnRecordsDisplay()) / oTable.fnSettings()._iDisplayLength) > 1 ) {
						$('.dataTables_paginate').removeClass('hide');
					} else {
						$('.dataTables_paginate').addClass('hide');
					}
				};
			}
		},
		// Modify the generate HTML for datatables to have better Bootstrap styling
		modifyDatatableHtml = function($elem, options, $scope) {
			// remove the <label> around the input
			$('.dataTables_filter > label').replaceWith(function() {
				return $('input', this);
			});
			// add the icon before the input (bootstrap styling)
			$('.dataTables_filter input').before('<span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>');
			// set the input's placeholder text
			$('.dataTables_filter input').attr('placeholder', options.filterPlaceholder).addClass('form-control');
			// add the loading spinner, which needs to be $compiled to make use of the scope
			$('.dataTables_filter').after($compile(options.filterHtml + (options.noSpinner?'':'<img class="spinner" ng-show="dtOptions.isLoading" src="'+DTConfig.getSpinnerImg()+'">'))($scope));
		},
		// If there is a callback defined for clicking a row, then here is where it will be added
		addRowClickCallback = function($elem, options) {
			if (angular.isFunction(options.fnRowClickCallback)) {
				angular.element('#'+$elem[0].id+' tbody').on('click', 'tr', function() {
					options.fnRowClickCallback(oTable.fnGetData(this), oTable.fnGetPosition(this), this);
				});
			}
		},
		// Render the table via a $timeout to be sure that angular has finished rendering before calling datatables
		initDataTable = function($elem, options, $scope) {
			$timeout(function() {
				addPreAndPostCallbacks($elem, options, $scope);
				oTable = $elem.dataTable(options).fnSetFilteringDelay();
				modifyDatatableHtml($elem, options, $scope);
				// Show $elem
				$elem.show();
				addRowClickCallback($elem, options, $scope);
			}, 0, false);
		}
	;

	return {
		restrict: 'A',
		scope: {
			dtOptions: '=',
			dtColumns: '='
		},
		link: function($scope, $elem) {
			// Hide the table
			$elem.hide();
			// Build options
			var options;
			if (angular.isDefined($scope.dtOptions)) {
				options = {};
				angular.extend(options, $scope.dtOptions);
				// Set the columns
				if (angular.isArray($scope.dtColumns)) {
					options.aoColumns = $scope.dtColumns;
				}
			}
			// Init datatables
			initDataTable($elem, options, $scope);
			// Add watchers
			$scope.$watch('dtOptions.reload', function (reload) {
				if (reload) {
					$scope.dtOptions.reload = false;
					if (oTable) {
						oTable.fnDraw(false);
					}
				}
			});
			// When the table is destroyed...
			$scope.$on('$destroy', function() {
				oTable.fnDestroy();
			});
		}
	};
}])

;
angular.module('datatables.factory', [
	'datatables.bootstrap'
])

// Default options for DataTables
.value('DTDefaultOptions', {
	'sDom': '<\'row\'<\'col-xs-12\'f>r><\'table-responsive\'t><\'row\'<\'col-xs-6\'i><\'col-xs-6\'p>>',
	'oLanguage': {
		'sSearch': '',
		'sNext': '&raquo;',
		'sPrevious': '&laquo;'
	},
	'bServerSide': true,
	'bAutoWidth': false,
	'filterPlaceholder': 'Filter',
	'filterHtml': '',
	'noSpinner': false,
	'extraParams': []
})

// Builds the options object needed for Datatables.
.factory('DTOptionsBuilder', ['DTBootstrap', 'DTDefaultOptions', function(DTBootstrap, DTDefaultOptions) {
	/**
	 * Returns an object containing the request data.
	 * @param data the request data
	 * @param options an instance of DTOptions
	 */
	var transformRequestData = function(data, options) {
		var result = {}, length = data.length;
		for (var i = 0; i < length; i++) {
			result[data[i].name] = data[i].value;
		}
		if (options.extraParams) {
			var extras = options.extraParams;
			length = extras.length;
			for (var j = 0; j < length; j++) {
				result[extras[j].name] = extras[j].value;
			}
		}
		return result;
	},
	/**
	 * The datatables options class
	 * @param sAjaxSource the ajax source to fetch the data
	 * @param dataPromise the promise to fetch the data
	 */
	DTOptions = function() {
		// indicates if the table data needs to be reloaded
		this.reload = false;
		// toggles the reload indicator
		this.reloadData = function () {
			this.reload = true;
			return this;
		};
		// sets the values of extraParams on the options object
		this.setExtraParams = function (extras) {
			this.extraParams = extras || [];
			return this;
		};
		/**
		 * Add the option to the datatables optoins
		 * @param key the key of the option
		 * @param value an object or a function of the option
		 * @returns {DTOptions} the options
		 */
		this.withOption = function(key, value) {
			if (!angular.isString(key)) {
				throw new Error('The key must be a string');
			}
			this[key] = value;
			return this;
		};
		/**
		 * Set the placeholder text for the filter input.
		 * This is not an actual datatables option, but a custom option for this module.
		 * @param placeholder the placeholder text to use
		 * @returns {DTOptions} the options
		 */
		this.withPlaceholderText = function(placeholder) {
			if (!angular.isString(placeholder)) {
				throw new Error('The placeholder text must be a String');
			}
			this.filterPlaceholder = placeholder;
			return this;
		};
		/**
		 * Set the row click callback function.
		 * This is not an actual datatables option, but a custom option for this module.
		 * @param fn the function for the callback
		 * @returns {DTOptions} the options
		 */
		this.withFnRowClickCallback = function(fn) {
			if (!angular.isFunction(fn)) {
				throw new Error('The parameter must be a function');
			}
			this.fnRowClickCallback = fn;
			return this;
		};
		/**
		 * Set the pagination type.
		 * @param sPaginationType the pagination type
		 * @returns {DTOptions} the options
		 */
		this.withPaginationType = function(sPaginationType) {
			if (!angular.isString(sPaginationType)) {
				throw new Error('The pagination type must be provided');
			}
			this.sPaginationType = sPaginationType;
			return this;
		};
		/**
		 * Set the language of the datatables
		 * @param oLanguage the language
		 * @returns {DTOptions} the options
		 */
		this.withLanguage = function(oLanguage) {
			this.oLanguage = oLanguage;
			return this;
		};
		/**
		 * Set default number of items per page to display
		 * @param iDisplayLength the number of items per page
		 * @returns {DTOptions} the options
		 */
		this.withDisplayLength = function(iDisplayLength) {
			this.iDisplayLength = iDisplayLength;
			return this;
		};
		/**
		 * Add the ajax data properties.
		 * @param sAjaxDataProp the ajax data property
		 * @returns {DTOptions} the options
		 */
		this.withDataProp = function(sAjaxDataProp) {
			this.sAjaxDataProp = sAjaxDataProp;
			return this;
		};
		/**
		 * Set the server data function.
		 * @param fn the function of the server retrieval
		 * @returns {DTOptions} the options
		 */
		this.withFnServerData = function(fn) {
			if (!angular.isFunction(fn)) {
				throw new Error('The parameter must be a function');
			}
			this.fnServerData = fn;
			return this;
		};
		/**
		 * Uses the $resource service as a data source for fnServerData.
		 * @param fn the $resource service method to use
		 * @returns {DTOptions} the options
		 */
		this.withServiceMethod = function(fn) {
			var options = this;
			if (!angular.isFunction(fn)) {
				throw new Error('The parameter must be a function');
			}
			return this.withFnServerData(function(sSource, aoData, fnCallback) {
				fn(transformRequestData(aoData, options), function(json) {
					fnCallback(json);
				});
			});
		};
		/**
		 * Set the pre draw callback function.
		 * @param fn the function for the callback
		 * @returns {DTOptions} the options
		 */
		this.withFnPreDrawCallback = function(fn) {
			if (!angular.isFunction(fn)) {
				throw new Error('The parameter must be a function');
			}
			this.fnPreDrawCallback = fn;
			return this;
		};
		/**
		 * Set the draw callback function.
		 * @param fn the function for the callback
		 * @returns {DTOptions} the options
		 */
		this.withFnDrawCallback = function(fn) {
			if (!angular.isFunction(fn)) {
				throw new Error('The parameter must be a function');
			}
			this.fnDrawCallback = fn;
			return this;
		};
		/**
		 * Add bootstrap compatibility
		 * @returns {DTOptions} the options
		 */
		this.withBootstrap = function() {
			DTBootstrap.integrate(this);
			return this;
		};
		/**
		 * Add extra filters to the filter area
		 * @param html the html of the additional filters (will go through $compile)
		 * @returns {DTOptions} the options
		 */
		this.withAdditionalFilters = function(html) {
			this.filterHtml = html;
			return this;
		};
		/**
		 * Toggles whether a spinner gif should be shown in the filter area.
		 * @param noSpinner set to true if you want to remove the html for adding a spinner
		 * @returns {DTOptions} the options
		 */
		this.withoutSpinner = function(noSpinner) {
			this.noSpinner = noSpinner;
			return this;
		};
	};

	// Return the DTOptionsBuilder
	return {
		newOptions: function() {
			return new DTOptions();
		},
		withDefaultOptions: function() {
			return angular.extend(new DTOptions(), DTDefaultOptions);
		}
	};
}])

// Builds a column option object needed by Datatables when defining a column.
.factory('DTColumnBuilder', function() {
	/**
	 * The datatables column 
	 * @param mData the data to display of the column
	 * @param sTitle the sTitle of the column title to display in the DOM
	 */
	var DTColumn = function(mData, sTitle) {
		if (angular.isUndefined(mData)) {
			throw new Error('The parameter "mData" is not defined!');
		}
		this.mData = mData;
		this.sTitle = sTitle || '';
		/**
		 * Add the option of the column
		 * @param key the key of the option
		 * @param value an object or a function of the option
		 * @returns {DTColumn} the wrapped datatables column
		 */
		this.withOption = function(key, value) {
			if (!angular.isString(key)) {
				throw new Error('The key must be a string');
			}
			this[key] = value;
			return this;
		};
		/**
		 * Set the title of the colum
		 * @param sTitle the sTitle of the column
		 * @returns {DTColumn} the wrapped datatables column
		 */
		this.withTitle = function(sTitle) {
			this.sTitle = sTitle;
			return this;
		};
		/**
		 * Set the CSS class of the column
		 * @param sClass the CSS class
		 * @returns {DTColumn} the wrapped datatables column
		 */
		this.withClass = function(sClass) {
			this.sClass = sClass;
			return this;
		};
		/**
		 * Set the width of the column
		 * @param sWidth the width as a string
		 * @returns {DTColumn} the wrapped datatables column
		 */
		this.withWidth = function(sWidth) {
			this.sWidth = sWidth;
			return this;
		};
		/**
		 * Hide the column
		 * @returns {DTColumn} the wrapped datatables column
		 */
		this.notVisible = function() {
			this.bVisible = false;
			return this;
		};
		/**
		 * Set the column as not sortable
		 * @returns {DTColumn} the wrapped datatables column
		 */
		this.notSortable = function() {
			this.bSortable = false;
			return this;
		};
		/**
		 * Render each cell with the given parameter
		 * @mRender mRender the function/string to render the data
		 * @returns {DTColumn} the wrapped datatables column
		 */
		this.renderWith = function(mRender) {
			this.mRender = mRender;
			return this;
		};
	};
	
	// Return the DTColumnBuilder
	return {
		addColumn: function(mData, sTitle) {
			return new DTColumn(mData, sTitle);
		}
	};
})

;
angular.module('forge.datatables', [
	// Module children
	'datatables.factory',
	'datatables.directive'
])

/**
 * A provier that allows the spinner image to be set during configuration.
 */
.provider('DTConfig', function() {
	var spinnerImg = 'assets/img/spinner.gif';

	this.$get = function() {
		return {
			getSpinnerImg: function() {
				return spinnerImg;
			}
		};
	};

	this.setSpinnerImage = function(newImg) {
		spinnerImg = newImg;
	};
})

;

/**
 * Add a delay to the input filter before updating the table.
 * Ideally this should be written in AngularJS and not jQuery
 */
$.fn.dataTableExt.oApi.fnSetFilteringDelay = function(oSettings, iDelay) {
	var _that = this;
	if (iDelay === undefined) {
		iDelay = 250;
	}

	this.each(function(i) {
		$.fn.dataTableExt.iApiIndex = i;
		var oTimerId = null,
			sPreviousSearch = null,
			anControl = $('input', _that.fnSettings().aanFeatures.f || '.dataTables_filter')
		;
		anControl.unbind('keyup').bind('keyup', function() {
			if (sPreviousSearch === null || sPreviousSearch !== anControl.val()) {
				window.clearTimeout(oTimerId);
				sPreviousSearch = anControl.val();
				oTimerId = window.setTimeout(function() {
					$.fn.dataTableExt.iApiIndex = i;
					_that.fnFilter(anControl.val());
				}, iDelay);
			}
		});
		return this;
	});
	return this;
};

})(window, window.$, window.angular);