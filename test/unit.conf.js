// Karma configuration for Unit tests
// http://karma-runner.github.io/0.10/config/configuration-file.html
module.exports = function(config) {
	var conf = {};

	conf.basePath = '',
	conf.frameworks = ['jasmine'],
	conf.reporters = ['progress'],

	// Start these browsers, currently available:
	// - Chrome
	// - ChromeCanary
	// - Firefox
	// - Opera
	// - Safari (only Mac)
	// - PhantomJS
	// - IE (only Windows)
	conf.browsers = ['PhantomJS'],
	conf.autoWatch = true,

	// these are default values anyway
	conf.singleRun = false,
	conf.colors = true,

	// Extra files needed just for Unit tests (including the actual unit test files)
	conf.files = [
		//3rd Party Code
		'../src/vendor/jquery/jquery.js',
		'../src/vendor/jasmine-jquery/lib/jasmine-jquery.js',
		'../src/vendor/angular/angular.js',
		'../src/vendor/angular-mocks/angular-mocks.js',
		'../src/vendor/datatables/media/js/jquery.dataTables.js',
		
		//App source files
		'../src/js/**/*.module.js',
		'../src/js/**/*.js',

		//test files
		'unit/**/*.js'
	];

	// level of logging
	// possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
	logLevel: config.LOG_DEBUG

	config.set(conf);
};