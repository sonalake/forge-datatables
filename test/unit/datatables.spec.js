describe("Forge Datatables", function() {
	'use strict';

	// load the module
	beforeEach(module('forge.datatables'));

	/**
	 * Tests the DTConfig provider.
	 */
	describe('DTConfig Provider', function () {
		var provider;

		beforeEach(module('forge.datatables', function(DTConfigProvider) {
			provider = DTConfigProvider;
		}))

		it('should be properly instantiated', inject(function() {
			expect(provider.setSpinnerImage).toBeDefined();
			expect(provider.$get).toBeDefined();
			expect(provider.$get().getSpinnerImg).toBeDefined();
		}));
		it('should have a default spinner image set', inject(function() {
			expect(provider.$get().getSpinnerImg()).toBe('assets/img/spinner.gif');
		}));
		it('should allow the user to set a value for the spinner image', inject(function() {
			expect(provider.setSpinnerImage).toBeDefined();
			provider.setSpinnerImage('somewhere/spinner.gif');
			expect(provider.$get().getSpinnerImg()).toBe('somewhere/spinner.gif');
		}));
	});

	/**
	 * Tests the DTDefaultOptions value.
	 */
	describe('DTDefaultOptions Value', function () {
		var value;

		beforeEach(function() {
			inject(function($injector) {
				value = $injector.get('DTDefaultOptions');
			});
		});

		it('should be properly instantiated', inject(function() {
			expect(value).toBeDefined();
			expect(value.sDom).toBe('<\'row\'<\'col-xs-12\'f>r><\'table-responsive\'t><\'row\'<\'col-xs-6\'i><\'col-xs-6\'p>>');
			expect(value.oLanguage).toBeDefined();
			expect(value.oLanguage.sSearch).toBe('');
			expect(value.oLanguage.sNext).toBe('&raquo;');
			expect(value.oLanguage.sPrevious).toBe('&laquo;');
			expect(value.bServerSide).toBeTruthy();
			expect(value.bAutoWidth).toBeFalsy();
			expect(value.filterPlaceholder).toBe('Filter');
			expect(value.filterHtml).toBe('');
			expect(value.noSpinner).toBeFalsy();
			expect(value.extraParams).toBeDefined();
			expect(value.extraParams.length).toBe(0);
		}));
	});

	/**
	 * Tests the DTOptionsBuilder factory.
	 */
	describe('DTOptionsBuilder Factory', function() {
		var DTOptionsBuilder;
		
		beforeEach(function() {
			inject(function($injector) {
				DTOptionsBuilder = $injector.get('DTOptionsBuilder');
			});
		});

		it('should be properly instantiated', function () {
			var dtOptions = DTOptionsBuilder.newOptions();
			expect(dtOptions).toBeDefined();
			expect(dtOptions.reload).toBeFalsy();
			expect(dtOptions.reloadData).toBeDefined();
			expect(dtOptions.setExtraParams).toBeDefined();
			expect(dtOptions.withOption).toBeDefined();
			expect(dtOptions.withPlaceholderText).toBeDefined();
			expect(dtOptions.withFnRowClickCallback).toBeDefined();
			expect(dtOptions.withPaginationType).toBeDefined();
			expect(dtOptions.withLanguage).toBeDefined();
			expect(dtOptions.withDisplayLength).toBeDefined();
			expect(dtOptions.withDataProp).toBeDefined();
			expect(dtOptions.withFnServerData).toBeDefined();
			expect(dtOptions.withServiceMethod).toBeDefined();
			expect(dtOptions.withFnPreDrawCallback).toBeDefined();
			expect(dtOptions.withFnDrawCallback).toBeDefined();
			expect(dtOptions.withBootstrap).toBeDefined();
			expect(dtOptions.withAdditionalFilters).toBeDefined();
			expect(dtOptions.withoutSpinner).toBeDefined();
		});
		it('should be properly instantiated with the default options', function () {
			var dtOptions = DTOptionsBuilder.withDefaultOptions();
			// the default options
			expect(dtOptions.sDom).toBe('<\'row\'<\'col-xs-12\'f>r><\'table-responsive\'t><\'row\'<\'col-xs-6\'i><\'col-xs-6\'p>>');
			expect(dtOptions.oLanguage).toBeDefined();
			expect(dtOptions.oLanguage.sSearch).toBe('');
			expect(dtOptions.oLanguage.sNext).toBe('&raquo;');
			expect(dtOptions.oLanguage.sPrevious).toBe('&laquo;');
			expect(dtOptions.bServerSide).toBeTruthy();
			expect(dtOptions.bAutoWidth).toBeFalsy();
			expect(dtOptions.filterPlaceholder).toBe('Filter');
			expect(dtOptions.filterHtml).toBe('');
			expect(dtOptions.noSpinner).toBeFalsy();
			expect(dtOptions.extraParams).toBeDefined();
			expect(dtOptions.extraParams.length).toBe(0);

			// the regular set of variables and functions
			expect(dtOptions.reload).toBeFalsy();
			expect(dtOptions.reloadData).toBeDefined();
			expect(dtOptions.setExtraParams).toBeDefined();
			expect(dtOptions.withOption).toBeDefined();
			expect(dtOptions.withPlaceholderText).toBeDefined();
			expect(dtOptions.withFnRowClickCallback).toBeDefined();
			expect(dtOptions.withPaginationType).toBeDefined();
			expect(dtOptions.withLanguage).toBeDefined();
			expect(dtOptions.withDisplayLength).toBeDefined();
			expect(dtOptions.withDataProp).toBeDefined();
			expect(dtOptions.withFnServerData).toBeDefined();
			expect(dtOptions.withServiceMethod).toBeDefined();
			expect(dtOptions.withFnPreDrawCallback).toBeDefined();
			expect(dtOptions.withFnDrawCallback).toBeDefined();
			expect(dtOptions.withBootstrap).toBeDefined();
			expect(dtOptions.withAdditionalFilters).toBeDefined();
			expect(dtOptions.withoutSpinner).toBeDefined();
		});

		describe('DTOptions functions', function() {
			var dtOptions;

			beforeEach(function() {
				dtOptions = DTOptionsBuilder.newOptions();
			});

			it('reloadData() should set dtOptions.reload to true', function () {
				expect(dtOptions.reload).toBeFalsy();
				dtOptions.reloadData();
				expect(dtOptions.reload).toBeTruthy();
			});
			it('setExtraParams(...) should set dtOptions.extraParams', function () {
				expect(dtOptions.extraParams).not.toBeDefined();
				dtOptions.setExtraParams([{name:'extra', value:'params'}, {name:'more', value:'things'}]);
				expect(dtOptions.extraParams).toBeDefined();
				expect(dtOptions.extraParams.length).toBe(2);
			});
			it('setExtraParams(...) without any params should set dtOptions.extraParams to an empty array', function () {
				expect(dtOptions.extraParams).not.toBeDefined();
				dtOptions.setExtraParams();
				expect(dtOptions.extraParams).toBeDefined();
				expect(dtOptions.extraParams.length).toBe(0);
			});
			it('withOption(...) should set the specified option in dtOptions', function () {
				expect(dtOptions.example).not.toBeDefined();
				dtOptions.withOption('example', 'value1');
				expect(dtOptions.example).toBe('value1');
			});
			it('withOption(...) should only accept String values for the key', function () {
				expect(function(){dtOptions.withOption(1234, 'value1');}).toThrow(new Error('The key must be a string'));
				expect(function(){dtOptions.withOption([], 'value1');}).toThrow(new Error('The key must be a string'));
			});
			it('withPlaceholderText(...) should set the value of dtOptions.filterPlaceholder', function () {
				expect(dtOptions.filterPlaceholder).not.toBeDefined();
				dtOptions.withPlaceholderText('example placeholder');
				expect(dtOptions.filterPlaceholder).toBe('example placeholder');
			});
			it('withPlaceholderText(...) should only accept String values for dtOptions.filterPlaceholder', function () {
				expect(dtOptions.filterPlaceholder).not.toBeDefined();
				expect(function(){dtOptions.withPlaceholderText(1234);}).toThrow(new Error('The placeholder text must be a String'));
				expect(function(){dtOptions.withPlaceholderText([]);}).toThrow(new Error('The placeholder text must be a String'));
				expect(dtOptions.filterPlaceholder).not.toBeDefined();
			});
			it('withFnRowClickCallback(...) should set the value of dtOptions.fnRowClickCallback', function () {
				var fn = function() {};
				expect(dtOptions.fnRowClickCallback).not.toBeDefined();
				dtOptions.withFnRowClickCallback(fn);
				expect(dtOptions.fnRowClickCallback).toBe(fn);
			});
			it('withFnRowClickCallback(...) should only accept Function values for dtOptions.fnRowClickCallback', function () {
				expect(dtOptions.fnRowClickCallback).not.toBeDefined();
				expect(function(){dtOptions.withFnRowClickCallback('function');}).toThrow(new Error('The parameter must be a function'));
				expect(function(){dtOptions.withFnRowClickCallback(1234);}).toThrow(new Error('The parameter must be a function'));
				expect(function(){dtOptions.withFnRowClickCallback([]);}).toThrow(new Error('The parameter must be a function'));
				expect(dtOptions.fnRowClickCallback).not.toBeDefined();
			});
			it('withPaginationType(...) should set the value of dtOptions.sPaginationType', function () {
				expect(dtOptions.sPaginationType).not.toBeDefined();
				dtOptions.withPaginationType('pagination type');
				expect(dtOptions.sPaginationType).toBe('pagination type');
			});
			it('withPaginationType(...) should only accept String values for dtOptions.sPaginationType', function () {
				expect(dtOptions.sPaginationType).not.toBeDefined();
				expect(function(){dtOptions.withPaginationType(1234);}).toThrow(new Error('The pagination type must be provided'));
				expect(function(){dtOptions.withPaginationType([]);}).toThrow(new Error('The pagination type must be provided'));
				expect(dtOptions.sPaginationType).not.toBeDefined();
			});
			it('withLanguage(...) should set the value of dtOptions.oLanguage', function () {
				var oLanguageObject = {'sSearch': ''};
				expect(dtOptions.oLanguage).not.toBeDefined();
				dtOptions.withLanguage(oLanguageObject);
				expect(dtOptions.oLanguage).toBe(oLanguageObject);
			});
			it('withDisplayLength(...) should set the value of dtOptions.iDisplayLength', function () {
				expect(dtOptions.iDisplayLength).not.toBeDefined();
				dtOptions.withDisplayLength(2);
				expect(dtOptions.iDisplayLength).toBe(2);
			});
			it('withDataProp(...) should set the value of dtOptions.sAjaxDataProp', function () {
				expect(dtOptions.sAjaxDataProp).not.toBeDefined();
				dtOptions.withDataProp('data');
				expect(dtOptions.sAjaxDataProp).toBe('data');
			});
			it('withFnServerData(...) should set the value of dtOptions.fnServerData', function () {
				var fn = function() {};
				expect(dtOptions.fnServerData).not.toBeDefined();
				dtOptions.withFnServerData(fn);
				expect(dtOptions.fnServerData).toBe(fn);
			});
			it('withFnServerData(...) should only accept Function values for dtOptions.fnServerData', function () {
				expect(dtOptions.fnServerData).not.toBeDefined();
				expect(function(){dtOptions.withFnServerData('function');}).toThrow(new Error('The parameter must be a function'));
				expect(function(){dtOptions.withFnServerData(1234);}).toThrow(new Error('The parameter must be a function'));
				expect(function(){dtOptions.withFnServerData([]);}).toThrow(new Error('The parameter must be a function'));
				expect(dtOptions.fnServerData).not.toBeDefined();
			});
			it('withServiceMethod(...) should set the value of dtOptions.fnServerData', function () {
				var fn = function() {};
				expect(dtOptions.fnServerData).not.toBeDefined();
				dtOptions.withServiceMethod(fn);
				expect(dtOptions.fnServerData).toBeDefined();
			});
			it('withServiceMethod(...) should only accept Function values for dtOptions.fnServerData', function () {
				expect(dtOptions.fnServerData).not.toBeDefined();
				expect(function(){dtOptions.withServiceMethod('function');}).toThrow(new Error('The parameter must be a function'));
				expect(function(){dtOptions.withServiceMethod(1234);}).toThrow(new Error('The parameter must be a function'));
				expect(function(){dtOptions.withServiceMethod([]);}).toThrow(new Error('The parameter must be a function'));
				expect(dtOptions.fnServerData).not.toBeDefined();
			});
			it('withFnPreDrawCallback(...) should set the value of dtOptions.fnPreDrawCallback', function () {
				var fn = function() {};
				expect(dtOptions.fnPreDrawCallback).not.toBeDefined();
				dtOptions.withFnPreDrawCallback(fn);
				expect(dtOptions.fnPreDrawCallback).toBe(fn);
			});
			it('withFnPreDrawCallback(...) should only accept Function values for dtOptions.fnPreDrawCallback', function () {
				expect(dtOptions.fnPreDrawCallback).not.toBeDefined();
				expect(function(){dtOptions.withFnPreDrawCallback('function');}).toThrow(new Error('The parameter must be a function'));
				expect(function(){dtOptions.withFnPreDrawCallback(1234);}).toThrow(new Error('The parameter must be a function'));
				expect(function(){dtOptions.withFnPreDrawCallback([]);}).toThrow(new Error('The parameter must be a function'));
				expect(dtOptions.fnPreDrawCallback).not.toBeDefined();
			});
			it('withFnDrawCallback(...) should set the value of dtOptions.fnDrawCallback', function () {
				var fn = function() {};
				expect(dtOptions.fnDrawCallback).not.toBeDefined();
				dtOptions.withFnDrawCallback(fn);
				expect(dtOptions.fnDrawCallback).toBe(fn);
			});
			it('withFnDrawCallback(...) should only accept Function values for dtOptions.fnDrawCallback', function () {
				expect(dtOptions.fnDrawCallback).not.toBeDefined();
				expect(function(){dtOptions.withFnDrawCallback('function');}).toThrow(new Error('The parameter must be a function'));
				expect(function(){dtOptions.withFnDrawCallback(1234);}).toThrow(new Error('The parameter must be a function'));
				expect(function(){dtOptions.withFnDrawCallback([]);}).toThrow(new Error('The parameter must be a function'));
				expect(dtOptions.fnDrawCallback).not.toBeDefined();
			});
			it('withBootstrap() should set DataTables to use Bootstrap for it\'s pagination', function () {
				expect(dtOptions.sPaginationType).not.toBeDefined();
				dtOptions.withBootstrap();
				expect(dtOptions.sPaginationType).toBe('bootstrap');
			});
			it('withAdditionalFilters(...) should set the value of dtOptions.filterHtml', function () {
				expect(dtOptions.filterHtml).not.toBeDefined();
				dtOptions.withAdditionalFilters('<div>hi</div>');
				expect(dtOptions.filterHtml).toBe('<div>hi</div>');
			});
			it('withoutSpinner(...) should set the value of dtOptions.noSpinner', function () {
				expect(dtOptions.noSpinner).not.toBeDefined();
				dtOptions.withoutSpinner(false);
				expect(dtOptions.noSpinner).toBeFalsy();
				dtOptions.withoutSpinner(true);
				expect(dtOptions.noSpinner).toBeTruthy();
			});
		});
	});

	/**
	 * Tests the DTColumnBuilder factory.
	 */
	describe('DTColumnBuilder', function() {
		var DTColumnBuilder;
		
		beforeEach(function() {
			inject(function($injector) {
				DTColumnBuilder = $injector.get('DTColumnBuilder');
			});
		});

		it('should be properly instantiated', function () {
			var column = DTColumnBuilder.addColumn('data', 'title');
			expect(column).toBeDefined();
			expect(column.mData).toBe('data');
			expect(column.sTitle).toBe('title');
			expect(column.withOption).toBeDefined();
			expect(column.withTitle).toBeDefined();
			expect(column.withClass).toBeDefined();
			expect(column.withWidth).toBeDefined();
			expect(column.notVisible).toBeDefined();
			expect(column.notSortable).toBeDefined();
			expect(column.renderWith).toBeDefined();
		});
		it('should allow for column.sTitle to be optional', function () {
			var column = DTColumnBuilder.addColumn('data');
			expect(column).toBeDefined();
			expect(column.mData).toBe('data');
			expect(column.sTitle).toBe('');
			expect(column.withOption).toBeDefined();
			expect(column.withTitle).toBeDefined();
			expect(column.withClass).toBeDefined();
			expect(column.withWidth).toBeDefined();
			expect(column.notVisible).toBeDefined();
			expect(column.notSortable).toBeDefined();
			expect(column.renderWith).toBeDefined();
		});
		it('should require a value for column.mData', function () {
			expect(function() {
				DTColumnBuilder.addColumn();
			}).toThrow(new Error('The parameter "mData" is not defined!'));
		});

		describe('DTOptions functions', function() {
			var column;

			beforeEach(function() {
				column = DTColumnBuilder.addColumn('data');
			});

			it('withOption(...) should set the specified option in column', function () {
				expect(column.example).not.toBeDefined();
				column.withOption('example', 'value1');
				expect(column.example).toBe('value1');
			});
			it('withOption(...) should only accept String values for the key', function () {
				expect(function(){column.withOption(1234, 'value1');}).toThrow(new Error('The key must be a string'));
				expect(function(){column.withOption([], 'value1');}).toThrow(new Error('The key must be a string'));
			});
			it('withTitle(...) should set the value of column.sTitle', function () {
				expect(column.sTitle).toBe('');
				column.withTitle('new title');
				expect(column.sTitle).toBe('new title');
			});
			it('withClass(...) should set the value of column.sClass', function () {
				expect(column.sClass).not.toBeDefined();
				column.withClass('actions');
				expect(column.sClass).toBe('actions');
			});
			it('withWidth(...) should set the value of column.sWidth', function () {
				expect(column.sWidth).not.toBeDefined();
				column.withWidth('20%');
				expect(column.sWidth).toBe('20%');
			});
			it('notVisible() should set the value of column.bVisible to be false', function () {
				expect(column.bVisible).not.toBeDefined();
				column.notVisible();
				expect(column.bVisible).toBeFalsy();
			});
			it('notSortable() should set the value of column.bSortable to be false', function () {
				expect(column.bSortable).not.toBeDefined();
				column.notSortable();
				expect(column.bSortable).toBeFalsy();
			});
			it('renderWith(...) should set the value of column.mRender', function () {
				var fn = function() {};
				expect(column.mRender).not.toBeDefined();
				column.renderWith(fn);
				expect(column.mRender).toBe(fn);
			});
		});
	});

	/**
	 * Tests the DTBootstrap service.
	 */
	describe('DTBootstrap Service', function() {
		var DTBootstrap, dtOptions;
		
		beforeEach(function() {
			inject(function($injector) {
				DTBootstrap = $injector.get('DTBootstrap');
				dtOptions = $injector.get('DTOptionsBuilder').newOptions();
			});
		});

		it('should enable bootstrap for the pagination', function () {
			expect(dtOptions.sPaginationType).not.toBeDefined();
			DTBootstrap.integrate(dtOptions)
			expect(dtOptions.sPaginationType).toBe('bootstrap');
		});
	});

	/**
	 * Tests the datatable directive.
	 */
	describe('datatable Directive', function() {
		describe('Basic Setup', function() {
			var elm, scope;

			beforeEach(inject(function($compile, $rootScope, DTOptionsBuilder, DTColumnBuilder) {
				scope = $rootScope.$new();
				
				scope.dtOptions = DTOptionsBuilder.withDefaultOptions()
					.withBootstrap()
				;
				scope.dtColumns = [
					DTColumnBuilder.addColumn('id', 'ID').withWidth('20%'),
					DTColumnBuilder.addColumn('firstname', 'Name').withWidth('40%').renderWith(function(value, type, object) {
						return object.firstname + ' ' + object.lastname;
					}),
					DTColumnBuilder.addColumn('email', 'Email').withWidth('40%').notSortable()
				];

				elm = $compile('<table datatable="" dt-options="dtOptions" dt-columns="dtColumns"></table>')(scope);
				scope.$apply();
				return elm;
			}));

			it('DataTables should hide the original table HTML', function() {
				expect(elm.attr('class')).toBe('ng-scope ng-isolate-scope');
				expect(elm.attr('style')).toBe('display: none; ');
			});
			it('DataTables should add a bunch of HTML', function() {
				expect(angular.element('.dataTables_wrapper')).toBeDefined();
				expect(angular.element('.dataTables_filter')).toBeDefined();
				expect(angular.element('.dataTables_paginate')).toBeDefined();
			});
		});
	});

	// Any actions that needs to be done after ALL of the tests (occurs for each test)
	afterEach(function() {
	});
});